FROM python:3.11-alpine

WORKDIR /app

RUN apk add --no-cache gcc musl-dev libffi-dev postgresql-dev

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

COPY docker/start-app.sh /usr/bin/start-app.sh
RUN chmod 555 /usr/bin/start-app.sh
RUN rm -rf docker

EXPOSE 6012 
