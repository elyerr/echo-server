# Echo Server  

A WebSocket server that broadcasts and listens to real-time events without a maximum event limit.  

This server uses the WebSocket standard to emit events, making it compatible with any application.  

It was designed for the [Laravel Framework](https://laravel.com/docs/10.x), but it is adaptable to any application that implements its own broadcasting system.  

It features clients developed for:
- **PHP**
- **Node.js**


#### CLIENTS 
- **Echo CLient js**: https://gitlab.com/elyerr/echo-client-js
- **Echo Client php**: https://gitlab.com/elyerr/echo-client-php


#### DIAGRAM
![FUNCIONALIDAD DE ECHO SERVER](assets/echo-server-diagram.svg)


#### CONFIG 

```sh
cp .env.default .env
```

##### 
```sh
#AUTHORIZATION SERVER
AUTH_HOST="https://auth.elyerr.xyz"
#DEFAULT ENDPOINT
AUTH_HOST_ENDPOINT="/broadcasting/auth"

#ORIGIN  (*) for all origins , Only my trusted domains "domain.xyz, domain2.xyz"
SOCKET_ORIGINS="*"
SOCKET_PING_INTERVAL=5
SOCKET_COMPRESSION="deflate"
SOCKET_MAX_QUEUE=100
SOCKET_MAX_SIZE=2097152 # 2MB
SOCKET_READ_LIMIT=2097152 # 2MB
SOCKET_WRITE_LIMIT=2097152 # 2MB
SOCKET_PING_TIMEOUT=10

#CONTAINER DB
DB_HOST=db
DB_PORT=5432
DB_DATABASE=echo-server
#For security reasons, change the username and password
DB_USERNAME=
DB_PASSWORD=
#DAYS
DB_LOG_RETENTION=15  

#If you have certificates, store them in the ssl folder and place the names in the keys
#CERT_FILE="cer_file.crt"
#KEY_FILE="key_file.key"
```

#### RUN SERVER

```
docker compose up -d
```

#### EXAMPLE PROXY WITH NGINX

```sh
server {

    listen 80;

    server_name websocket.domain.xyz;

    location /app {	

        proxy_pass http://127.0.0.1:6012; 
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "Upgrade";
            proxy_set_header X-Real-IP $remote_addr; 
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;  
            proxy_set_header X-Forwarded-Proto $scheme; 
            proxy_set_header Host $http_host;
    }
} 
```
 
# DEVELOPER
[Telegram](https://t.me/elyerr)