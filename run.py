import os
import subprocess
import sys

#Get the absolute path 
script_dir = os.path.dirname(os.path.abspath(__file__))

#Changing directory
os.chdir(script_dir) 

args = sys.argv # get all args
args.pop(0) # remove first element 
query = " ".join(args) # changing list to string

try: 
    command = ["python", "./src/echo-server.py", query]
    subprocess.run(command, check=True)
except KeyboardInterrupt as e:
    print(f"Echo server stopped")