import asyncio, websockets, ssl, os,requests,json, logging
from Exceptions.UnknownOrigin import UnknownOrigin
from Exceptions.Origins import Origins 
from Models.Client import *
from Models.Channel import * 
from dotenv import load_dotenv
from Models.Postgres import Peer, Site, Log
from datetime import datetime, timedelta
logging.basicConfig(level=logging.INFO)

load_dotenv()
class Server(Origins):  
    #Save all origins
    ORIGINS = set()
    
    #Constructor
    def __init__(self, **kwargs):
        super().__init__()  
        self.host = kwargs.get('host', "127.0.0.1")
        self.port = kwargs.get('port', "6010")
        self.ping_interval = os.getenv('SOCKET_PING_INTERVAL', 5)
        self.compression = os.getenv('SOCKET_COMPRESSION','deflate')
        self.max_queue = os.getenv('SOCKET_MAX_QUEUE', 20)
        self.max_size = os.getenv('SOCKET_MAX_SIZE', 1048576 )
        self.read_limit = os.getenv('SOCKET_READ_LIMIT', 1048576)
        self.write_limit = os.getenv('SOCKET_WRITE_LIMIT', 1048576)
        self.ping_timeout = os.getenv('SOCKET_PING_TIMEOUT',10)
        self.authorization = f"{os.getenv('AUTH_HOST')}{os.getenv('AUTH_HOST_ENDPOINT')}"  
    
    @staticmethod
    def clock(): 
        """Get the current time
        """
        time = datetime.utcnow()    
        return time.strftime("%Y-%m-%d %H:%M:%S")     
    
    def get_token(self, message : dict, client: Client = None):
        """Get the Authorization token in the current connection
        """
        authorization = client.authorization() 
        token = message.get('token')
        if authorization:
            return authorization
        return token
        
        
    def authorize_channel(self, client : Client, channel : str, token: str):
        """Authorize channels 
        """        
        data = {'channel_name' : channel }    

        headers = {
            'Accept' : 'application/json',
        }
        
        if token: 
            headers.update({'Authorization' :  token})  
        else:
            headers.update({'Cookie' : client.get_cookies()})  

        response = requests.post(self.authorization, data=data, headers= headers , verify=False)
        
        if response.ok: 
            logging.info(f"{Server.clock()} IP {client.get_ip()} has been authorized to the {channel} channel")
            Log( 
                uuid = uuid.uuid4(), 
                status="success", 
                description = f"{Server.clock()} IP {client.get_ip()} has been authorized to the {channel} channel"
            ).save(force_insert=True)
            
            return True    
        else: 
            try:
                response.raise_for_status()   
            except requests.HTTPError as e:
                logging.info(f"{Server.clock()} {channel} {e}")
                Log( 
                    uuid = uuid.uuid4(), 
                    status="warning", 
                    description = f"{Server.clock()} {channel} {e}"
                ) .save(force_insert=True)  
                
            return False         
            
    async def start(self, host= None, port= None): 
        """Run server 
        """    
        #Location to get the ssl cert
        SSL_PATH = f"{os.path.dirname(__file__)}/../ssl"
        
        #Verify the path
        if not os.path.exists(SSL_PATH):
            os.mkdir(SSL_PATH)
                    

        def protocol(): 
            """Internal Function to get the ssl keys
            """             
            try:
                CERT = os.path.join(SSL_PATH, os.getenv('CERT_FILE', ''))
                KEY = os.path.join(SSL_PATH, os.getenv('KEY_FILE', ''))                             
                ssl_protocol = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
                ssl_protocol.load_cert_chain(CERT, KEY)  
                return ssl_protocol
            except IsADirectoryError as e:
                return None
            except FileNotFoundError as e:
                return None
        
        #Run the websocket protocol
        try:
            
            async with websockets.serve(
                        self.echo, 
                        self.host, 
                        self.port, 
                        ssl=protocol(), 
                        ping_interval= int(self.ping_interval),
                        compression= self.compression,
                        max_queue= int(self.max_queue),
                        max_size= int(self.max_size),
                        read_limit= int(self.read_limit),
                        write_limit= int(self.write_limit),
                        ping_timeout= int(self.ping_timeout)
                        ):
                if protocol():
                    logging.info(f"{Server.clock()} PROTOCOL WSS ACTIVATED IN SECURE MODE")
                    Log( 
                        uuid = uuid.uuid4(), 
                        status="success", 
                        description = f"{Server.clock()} PROTOCOL WSS ACTIVATED IN SECURE MODE"
                     ).save(force_insert=True) 

                else:
                    logging.info(f"{Server.clock()} PROTOCOL WS ON INSECURE MODE")
                    Log( 
                        uuid = uuid.uuid4(), 
                        status="warning", 
                        description = f"{Server.clock()} PROTOCOL WS ON INSECURE MODE"
                     ).save(force_insert=True)  
                    
                    logging.info(f'{Server.clock()} Server is ready to start listening events')
                    Log( 
                        uuid = uuid.uuid4(), 
                        status="success", 
                        description = f'{Server.clock()} Server is ready to start listening events'
                        ).save(force_insert=True)  
                await asyncio.Future()
                
        except Exception as e:
            logging.error(f"Error {e}")
            Log( 
                uuid = uuid.uuid4(), 
                status="error", 
                description = f"Error {e}"
            ).save(force_insert=True)      
    
    async def authorization_client(self, websocket : websockets, message : dict):   
        """Verify and authorize to the users 
        """         
        if 'event' in message and message.get('event') == 'connected':         
            #Checking if the user exists
            client = Client.find(websocket) 
            
            #User not exist
            if not client:
                #Get data
                origin=self.add_origin(websocket.request_headers['Origin'])
                ip = websocket.remote_address[0]
                #Updating database
                Peer(uuid = uuid.uuid4(), domain=origin, ip=ip).save(force_insert=True)           
                
                #register clients
                client = Client(socket=websocket, origin=origin)   
                #updaterin database
                await self.count_client_by_domain()
                #report message
                logging.info(f"{Server.clock()} New device detected from ip {client.get_ip()} (Clients Connected : {len(Client.all())})")
                Log( 
                    uuid = uuid.uuid4(), 
                    status="success", 
                    description = f"{Server.clock()} New device detected from ip {client.get_ip()} (Clients Connected : {len(Client.all())})"
                ).save(force_insert=True)  

            #Get channel from incoming message
            channel = Channel.get_channel_from_request(message) 
            
            #get token 
            token = self.get_token(message, client) 
            
            #save token  
            if token:
                client.set_token(token)                  

            if Channel.is_private(channel) and channel not in client.get_private() and self.authorize_channel(client, channel, token):
                Channel(channel = channel)                 
                client.add_private(channel)               

            if Channel.is_public(channel) and channel not in client.get_public():
                logging.info(f"{Server.clock()} IP {client.get_ip()} has been authorized to the {channel} channel")
                Log( 
                    uuid = uuid.uuid4(), 
                    status="success", 
                    description = f"{Server.clock()} IP {client.get_ip()} has been authorized to the {channel} channel"
                ) .save(force_insert=True)  
                
                Channel(channel = channel)
                client.add_public(channel)             

    async def unsubscribe(self, websocket : websockets, message : dict): 
        """Remove disconnected users
        """ 
        if 'event' in message and message.get('event') == 'disconnected':     
            Client.remove(websocket)   
            logging.info(f"{Server.clock()} {Server.clock()} user removed (Clients Connected : {len(Client.all())})")
            Log( 
                uuid = uuid.uuid4(), 
                status="success", 
                description = f"{Server.clock()} {Server.clock()} user removed (Clients Connected : {len(Client.all())})"
             ) .save(force_insert=True)                      

    def broadcast(self, clients : set, message : dict):   
        """Dispatch events to all users
        """ 
        sockets = [client.get_socket() for client in clients]
        websockets.broadcast(sockets, json.dumps(message)) 
        logging.info(f"{Server.clock()} Sending {len(sockets)} Event ({message.get('event')}) to the channel ({message.get('channel')})")
        Log( 
            uuid = uuid.uuid4(), 
            status="success", 
            description = f"{Server.clock()} Sending {len(sockets)} Event ({message.get('event')}) to the channel ({message.get('channel')})"
        ).save(force_insert=True)  

    async def dispatch_events(self, message : dict):  
        """Dispatch events for channels
        """ 
        if Channel.is_private(message.get('channel')):
            clients = Client.get_private_clients()
            self.broadcast(clients, message)
        
        if Channel.is_public(message.get('channel')):
            clients = Client.get_public_clients()
            self.broadcast(clients, message) 
            

    async def echo(self, websocket):
        """Echo process
        Args:
            websocket (websocket): WebSocket instance
        """        
        #prevent duplicated events
        lock_message = dict() 
        
        try:
           async for message in websocket: 
                
                #Deny access
                await Origins.deny_access(websocket) 
                
                await self.remove_clients()
                
                #Add global sets
                await self.set_origins(websocket.request_headers['Origin'])
                
                await self.pong(websocket, message)
                
                await self.remove_logs()
                
                #dispatch events      
                if 'type' not in message and lock_message != message:
                    
                    # save current message
                    lock_message = message
                    
                    #decoding message
                    message: dict = json.loads(message)                                         
                                        
                    await self.authorization_client(websocket, message)
                    
                    #Deleting token if it exists 
                    if message.get('token'): 
                        message.pop('token')

                    await self.unsubscribe(websocket, message)                        
                
                    await self.dispatch_events(message)                       
                        
                
        except UnknownOrigin as e:
            logging.error(f"{Server.clock()} Unknown Origin from ip {websocket.remote_address[0]}:{websocket.remote_address[1]}")
            Log( 
                uuid = uuid.uuid4(), 
                status="warning", 
                description = f"{Server.clock()} Unknown Origin from ip {websocket.remote_address[0]}:{websocket.remote_address[1]}"
            ).save(force_insert=True)  
        
        except websockets.exceptions.ConnectionClosedError as e:     
            logging.error(f"{Server.clock()} Connection was  closed {e.reason} {e.code}")
            Log( 
                uuid = uuid.uuid4(), 
                status="error", 
                description = f"{Server.clock()} Connection was closed {e}"
            ).save(force_insert=True)  
    
    async def pong(self, websocket: websockets, message: dict):
        """Ping pong function
        """
        try:        
            if 'type' in message: 
                await websocket.send(json.dumps({'type' : 'pong' }))
        except websockets.exceptions.ConnectionClosedOK as e:
            logging.error(f"{Server.clock()} Connection was closed {e}")
    
    async def remove_clients(self):
        #Remove disconnected users
        disconnected = Client.get_clients_disconnected()         
        for client in disconnected:
            Client.remove(client.get_socket())
            logging.info(f"Client disconnected  (Clients Connected : {len(Client.all())})")
            Log( 
                uuid = uuid.uuid4(), 
                status="warning", 
                description = f"Client disconnected  (Clients Connected : {len(Client.all())})"
            ).save(force_insert=True)  
            
            #updating database
            await self.count_client_by_domain()
    
    def add_origin(self, origin: str):
        """Save all origins
        """
        if origin.startswith("https://"):
            origin = origin[len("https://"):]

        if origin.startswith("http://"):
            origin = origin[len("http://"):]

        origin = origin.lstrip("www.") 
        
        return origin   
    
    async def set_origins(self, origin: str):     
        self.ORIGINS.add(self.add_origin(origin))
        
    async def count_client_by_domain(self):
        """count client by domain
        """        
        for origin in self.ORIGINS:
            count = 0
            for client in Client.get_clients_connected():  
                if origin == client.get_origin():
                   count += 1 
            #change to insert to database
            Site(uuid = uuid.uuid4(), domain=origin, peers= count).save(force_insert=True)        
    
    async def remove_logs(self):
        """Remove logs
        """
        retention = datetime.now() - timedelta(days=int(os.getenv('DB_LOG_RETENTION', 30)))
        
        logs = Log().delete().where(Log.created_at < retention).execute()
        if(logs > 0):
            logging.warning(f"{logs} was removed")
            Log( 
                    uuid = uuid.uuid4(), 
                    status="warning", 
                    description = f"{logs} was removed"
                ).save(force_insert=True)