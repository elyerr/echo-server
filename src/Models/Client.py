import json
import uuid
from websockets.frames import (   
    prepare_data,
)
class Client():
    
    CLIENTS = set()  
    
    def __init__(self, **kwargs ): 
        self.socket = kwargs.get('socket') 
        self.private = self.transform_data(kwargs.get('private', {}))
        self.public = self.transform_data(kwargs.get('public', {})) 
        self.origin = kwargs.get('origin')
        self.id = uuid.uuid4()
        self.__collect(self)

    def get_id(self):
        return self.id
    
    def get_socket(self): 
        return self.socket
    
    def set_token(self, token: str):
        self.token = token 
    
    def get_private(self):
        return self.private
    
    def get_public(self):
        return self.public 
    
    def get_origin(self):
        return self.origin
       
    def add_public(self, data):
        self.public.update(self.transform_data(data))         
    
    def add_private(self, data):
        self.private.update(self.transform_data(data))
        
    def add_origin(self, origin: str):
        self.origin = origin
             
    def __collect(self, data):
        self.CLIENTS.add(data)
        
    def to_string(self):        
        return { 
            'id': self.id,  
            'ip' : self.get_ip(),
            'port' : self.get_port(),
            'socket' : self.socket,
            'private' : self.private, 
            'public': self.public,
            'origin' : self.origin
            }
        
    def transform_data(self, data):  
        if type(data) == str: 
            return set(data.replace(' ', '').split(','))
        elif data is None:
            return {} 
        return set(data)
    
    def get_headers(self):
        return self.get_socket().request_headers
    
    def get_cookies(self):
        try :
            return self.get_socket().request_headers['Cookie']
        except KeyError as e:
            return None 
        
    def authorization(self):
        try :
            return self.get_socket().request_headers['Authorization']
        except KeyError as e:
            return None
     
    async def send(self, data: dict):
        await self.get_socket().send(json.dumps(data))
    
    def get_closed(self):
        return self.get_socket().closed
    
    def get_close_code(self):
        return self.get_socket().close_code
    
    def get_close_reason(self):
        return self.get_socket().close_reason
    
    def get_subprotocol(self):
        return self.get_socket().subprotocol
        
    def get_ip(self):
        return self.get_socket().remote_address[0]
        
    def get_port(self):
       return self.get_socket().remote_address[1]
       
    def get_extensions(self):
        return self.get_socket().extensions
    
    def get_state(self):
        return self.get_socket().state
         
    def write_frame_sync(self, data:dict):
        opcode, data = prepare_data(json.dumps(data))
        self.get_socket().write_frame_sync(True, opcode, data)
         
    @staticmethod
    def all():
        return Client.CLIENTS
    
    @staticmethod
    def find(socket):
        for c in Client.all():
            if socket == c.get_socket():
                return c
    
    @staticmethod
    def find_by_id(id):
        for c in Client.all():
            if id == c.get_id():
                return c
    
    @staticmethod
    def remove(socket):
        clients = list(Client.all())  # Convertimos el conjunto a una lista
        for c in clients:
            if socket == c.get_socket():
                Client.all().remove(c)

    @staticmethod
    def get_public_clients():
        clients  = set() 
        public_clients = [client for client in Client.all() if not client.get_closed() and len(client.get_public()) > 0]
        clients.update(client for client in public_clients)
        return clients
    
    @staticmethod
    def get_private_clients():
        clients = set()
        private_clients = [client for client in Client.all() if not client.get_closed() and len(client.get_private()) > 0]
        clients.update(client for client in private_clients)
        return clients
    
    @staticmethod
    def get_clients_disconnected():
        return [client for client in Client.all() if client.get_closed()]
    
    @staticmethod
    def get_clients_connected():
        return [client for client in Client.all() if client.get_state() == 1]