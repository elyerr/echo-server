import uuid
from dotenv import load_dotenv
import os
from peewee import *
load_dotenv()

  
database = PostgresqlDatabase(
        os.getenv('DB_DATABASE'),
        user= os.getenv('DB_USERNAME'),
        password= os.getenv('DB_PASSWORD'),
        host=os.getenv('DB_HOST'),
        port= os.getenv('DB_PORT')
    ) 

class Postgres(Model):    
    uuid = UUIDField(primary_key=True)
    created_at = TimestampField()
    
    class Meta:
        database = database

class Peer(Postgres):
    """To save every conection by user
    """
    ip = IPField(index=True)
    domain = CharField(max_length=200)


class Site(Postgres):
    """To save all conetion by domain
    """ 
    peers = IntegerField()
    domain = CharField(max_length=200)

class Log(Postgres):
    """Lo to save all logs
    """ 
    status = CharField(max_length=50)
    description = TextField()
    
class Migration():
    
    @staticmethod
    def create_tables():
        database.connect()
        with database:
            database.create_tables([Peer, Site, Log])
        print("Successful migration")    
    
    
  