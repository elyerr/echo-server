import uuid
class Event():
    pass
    """SQL = MySQLConnect()
    SQL.use_database()    
    TABLE = SQL.EVENT
    
    def __init__(self, **kwargs): 
        self.id = kwargs.get('id',str(uuid.uuid4()))
        self.event = kwargs.get('event', None)
        self.amount = kwargs.get('amount', None)
        self.channel = kwargs.get('channel', None)
        self.created_at = kwargs.get('created_at', None)       
     
    def save(self): 
        values = (self.get_id(), self.get_event(), self.get_amount(), self.get_channel())
        query = f"INSERT INTO {self.TABLE} (id, event, amount, channel) VALUES (%s, %s, %s, %s)"
        self.SQL.cursor.execute(query, values)
        self.SQL.open.commit()
        return self.SQL.cursor.rowcount
    
    def get_id(self):
        return self.id
        
    def get_amount(self):
        return self.amount
    
    def get_channel(self):
        return self.channel
    
    def get_event(self):
        return self.event
    
    def get_created_at(self):
        return self.created_at
        
    def is_string(self, string):
        if type(string) == str:
            return string
        return None
    
    def to_string(self):
        return {
            'id' : self.id,
            'event': self.event,
            'amount' : self.amount,
            'channel' : self.channel,
            'created_at' : self.created_at.strftime("%Y-%m-%d %H:%M:%S")         
        } 
    
    @staticmethod
    def all():
        events = []
        query = f"SELECT * FROM {Event.TABLE}"
        Event.SQL.cursor.execute(query)
        for event in Event.SQL.cursor.fetchall():
            events.append(Event(
                                 id = event[0],
                                 event = event[1],
                                 amount = event[2],
                                 channel = event[3],
                                 created_at = event[4]
            ))
        return events"""