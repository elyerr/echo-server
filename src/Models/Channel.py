class Channel(): 
    
    CHANNEL = set()
    
    def __init__(self, channel: str):
        self.CHANNEL.add(channel)
                
    @staticmethod
    def all():
        """Get all channels
        """
        return Channel.CHANNEL
     
    @staticmethod
    def is_private(channel):
        """Checking the private channel
        """
        return "private" in channel
    
    def is_public(channel):  
        """Checking the public channel
        """      
        return "public" in channel
    
    @staticmethod
    def get_channel_from_request(message :  dict):  
        """Get channel from message in
        """   
        return  message.get('channel')