
class UnknownOrigin(Exception):
    """Excepcion para origines desconociddos

    Args:
        Exception (_type_): Excepcion
    """
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
