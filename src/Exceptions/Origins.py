from Exceptions.UnknownOrigin import UnknownOrigin
from dotenv import load_dotenv
import os

load_dotenv()
class Origins():
    """Check origins 
    """
    @staticmethod
    async def deny_access(websocket):    

        def check_origins(origins, origin_request):
            for domain in origins:
                if domain in origin_request:
                    return True
            return False

        origins = os.getenv('SOCKET_ORIGINS').split(',')
        
        origin = websocket.request_headers['Origin']
    
        #Checking origin from request
        if (origin == os.getenv('AUTH_HOST')) or check_origins(origins, origin) or ('*' in origins): 
            pass
        else:   
            raise UnknownOrigin(f"request from {origin}")