
class Unauthorize(Exception):
    """Excepcion generada cuando el usuario no esta autorizado
    para acceder al canal

    Args:
        Exception (_type_): Excepcion
    """
    def __init__(self, *args: object) -> None:
        super().__init__(*args)    
    