import asyncio, sys , uuid, peewee, logging
from Server import Server
from Models.Postgres import Migration, Log
logging.basicConfig(level=logging.INFO)

ws = None
HOST = None
PORT = None 

for arg in sys.argv:
    
    if '--host' in arg:
        HOST=arg.replace('--host=', '')
    if '--port' in arg:
        PORT=arg.replace('--port=', '')
    
    if '--migrate' in arg:
        Migration.create_tables()
        sys.exit(0)
    
    if arg in ['--help', '-h']:
        help = """
        Options:

            Custom HOST and PORT
                --host=127.0.0.1 --port=6012
            
            Load migrations:
                --migrate
        """
        print(help)
        sys.exit()
        
try: 
    if __name__ == "__main__":
        
        Migration.create_tables()
        
        if len(sys.argv) == 3 and HOST and PORT:
            ws = Server(host=HOST, port=PORT)  
        
        else:      
            ws = Server()
            
        asyncio.run(ws.start())            

except asyncio.exceptions.CancelledError as e:
    logging.error(f"{Server.clock()} Connection interrupted.")
    Log( 
        uuid = uuid.uuid4(), 
        status="error", 
        description =  f"{Server.clock()} Connection interrupted."
     ).save(force_insert=True)  


except KeyboardInterrupt as e: 
    logging.error(f"{Server.clock()} Connection closed.")
    Log( 
        uuid = uuid.uuid4(), 
        status="error", 
        description = f"{Server.clock()} Connection closed."
     ).save(force_insert=True)    
except peewee.ProgrammingError as e:
    print(f"error { e }" )